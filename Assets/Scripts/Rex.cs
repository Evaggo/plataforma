﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Rex : MonoBehaviour
{
    public float maxS = 11f;
    public float fuerzaSalto = 5.0f;

    private Rigidbody2D rb2d = null;
    
    //private bool attack;    
    private bool isGrounded = false;
    private bool jump;
    private bool isJumping;
    private bool isDead;

    private float move = 0f;
    private float scalaActual;

    [SerializeField] private GameObject graphics;
    [SerializeField] private Animator animator;

    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        rb2d.AddForce(Vector2.right * move * maxS);

        animator.SetFloat("Speed", Math.Abs(rb2d.velocity.x));
    }

    private void Update()
    {
        if (isDead == false) {
            move = Input.GetAxis("Horizontal");
            jump = Input.GetKeyDown(KeyCode.Space);
            //attack = Input.GetButton("Fire1");

            /*if (attack && isGrounded)
            {
                move = 0f;
                jump = false;
                animator.SetBool("attack", attack);
            }
            else
            {
                animator.SetBool("attack", false);
            }*/


            if (jump && isGrounded)
            {
                isJumping = true;
                //Debug.LogError("Salta");
                jump = false;
                rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
                animator.SetBool("Jumping", isJumping);          
            } 

            scalaActual = graphics.transform.localScale.x;

            if (move > 0 && scalaActual < 0)
            {
                graphics.transform.localScale = new Vector3(1, 1, 1);
            }
            else if (move < 0 && scalaActual > 0)
            {
                graphics.transform.localScale = new Vector3(-1, 1, 1);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isGrounded = true;
        isJumping = false;
        animator.SetBool("Jumping", isJumping);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isGrounded = false;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Muerte") {
            animator.SetTrigger("Dead");
            isDead = true;
        }

        if (other.gameObject.tag == "PowerUp") {
            maxS *=4;
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "Trampolin") {
            isJumping = true; 
            jump = false;
            rb2d.AddForce(Vector2.up * fuerzaSalto*2, ForceMode2D.Impulse);
            animator.SetBool("Jumping", isJumping);
        }
    }

    public void SonidoMuerte () {

    }
}
