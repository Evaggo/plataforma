﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range : MonoBehaviour
{
    [SerializeField] float rangeDistance;
    [SerializeField] Transform player;
    [SerializeField] public float enemySpeed;
    [SerializeField] Patrullero patrol;

    private void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, rangeDistance);
    }

    private void Update() 
    {
        if (Mathf.Abs(Vector2.Distance(player.position, transform.position)) < rangeDistance)
        {
            patrol.enabled = false;
            transform.position = Vector2.MoveTowards(transform.position, player.position, Time.deltaTime*enemySpeed);
        } else 
        {
            patrol.enabled = true;
        }
    }
}
